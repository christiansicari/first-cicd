
import re
import os
from flask import Flask
from flask import render_template
import socket
import random
import os


color =  re.sub("[^0-9a-zA-Z]+", "", open("/app/color.txt", "r").read())
version =  ", ".join([re.sub("[^0-9a-zA-Z]+", "", v) for v in open("/app/version.txt","r").read().split(",")])
dummyenv =  os.environ.get('DUMMYENV', 'You forgot to set env variable')
app = Flask(__name__)

color_codes = {
    "red": "#e74c3c",
    "green": "#16a085",
    "blue": "#2980b9",
    "blue2": "#30336b",
    "pink": "#be2edd",
    "darkblue": "#130f40"
}


@app.route("/")
def main():
    #return 'Hello'
    print(color)
    return render_template('hello.html', name=socket.gethostname(), color=color_codes[color], version=version, dummyenv=dummyenv)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port="8080")
